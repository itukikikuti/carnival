﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class SpearAmylon : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField] GameObject spear;
    Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            StartCoroutine(Attack());
        }
    }

    IEnumerator Attack()
    {
        yield return new WaitForSeconds(Random.Range(0f, 0.3f));

        rb.AddForce(new Vector2(0f, 8f), ForceMode2D.Impulse);
        yield return new WaitForSeconds(0.5f);

        for (int i = 0; i < 1; i++)
        {
            GameObject go = Instantiate(spear, transform.position, Quaternion.identity);
            go.GetComponent<Spear>().Shoot(target);
            yield return new WaitForSeconds(0.2f);
        }
    }
}
