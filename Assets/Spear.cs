﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Spear : MonoBehaviour
{
    Rigidbody2D rb;
    float speed = 10f;

    public void Shoot(Transform target)
    {
        rb = GetComponent<Rigidbody2D>();
        Vector2 diff = target.position - transform.position;
        float angle = Mathf.Atan2(diff.y, diff.x);
        angle = 50f * Mathf.Deg2Rad;
        //angle += 45f * Mathf.Clamp01(diff.x / (speed * 2f)) * Mathf.Deg2Rad;
        angle += Random.Range(-10f, 10f) * Mathf.Deg2Rad;
        Vector2 velocity = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
        rb.velocity = velocity * speed;
    }

    void Update()
    {
        transform.right = rb.velocity;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        rb.velocity = Vector2.zero;
        rb.isKinematic = true;
        Destroy(this);
        Destroy(gameObject, 3f);
    }
}
