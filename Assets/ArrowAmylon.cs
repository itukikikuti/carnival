﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class ArrowAmylon : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField] GameObject arrow;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            StartCoroutine(Attack());
        }
    }

    IEnumerator Attack()
    {
        yield return new WaitForSeconds(Random.Range(0.2f, 0.5f));

        for (int i = 0; i < 3; i++)
        {
            GameObject go = Instantiate(arrow, transform.position, Quaternion.identity);
            go.GetComponent<Arrow>().Shoot(target);
            yield return new WaitForSeconds(0.5f);
        }
    }
}
