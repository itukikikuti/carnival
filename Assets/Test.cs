﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip rhythmSound;
    [SerializeField] AudioClip leftSound;
    [SerializeField] AudioClip rightSound;
    [SerializeField] int bpm = 110;

    IEnumerator Start()
    {
        while(true)
        {
            audioSource.PlayOneShot(rhythmSound);

            yield return new WaitForSeconds(1f / (bpm / 60f));
        }
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.LeftArrow))
            audioSource.PlayOneShot(leftSound);

        if (Input.GetKeyDown(KeyCode.RightArrow))
            audioSource.PlayOneShot(rightSound);
    }
}
